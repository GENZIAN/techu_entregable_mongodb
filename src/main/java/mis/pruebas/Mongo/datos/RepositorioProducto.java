package mis.pruebas.Mongo.datos;

import mis.pruebas.Mongo.Modelo.Producto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RepositorioProducto extends MongoRepository<Producto, String>
{
}
