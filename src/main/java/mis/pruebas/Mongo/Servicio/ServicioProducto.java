package mis.pruebas.Mongo.Servicio;

import mis.pruebas.Mongo.Modelo.Producto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ServicioProducto {
     // CRUD

    //POST /productos
    public Producto crearProducto(Producto producto);

    //GET /productos
    public List<Producto> obtenerProductos();

    //GET /productos/{id}
    public Optional<Producto> obtenerProductoPorId(String id);

    //PUT /productos/{id} RequestBody
    public Producto actualizarProducto(Producto producto);

    //DELETE /productos/{id}
    public void borrarProductoPorId(String id);
}
