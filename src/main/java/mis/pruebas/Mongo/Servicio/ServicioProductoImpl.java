package mis.pruebas.Mongo.Servicio;

import mis.pruebas.Mongo.Modelo.Producto;
import mis.pruebas.Mongo.datos.RepositorioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServicioProductoImpl implements ServicioProducto {

    public ServicioProductoImpl(){
        System.out.println("=============== ServicioProductoImpl");
    }

    @Autowired
    private RepositorioProducto repositorioProducto;

    @Override
    public Producto crearProducto(Producto producto) {
        return this.repositorioProducto.insert(producto);
    }

    @Override
    public List<Producto> obtenerProductos() {
        return this.repositorioProducto.findAll();
    }

    @Override
    public Optional<Producto> obtenerProductoPorId(String id) {
        return this.repositorioProducto.findById(id);
    }

    @Override
    public Producto actualizarProducto(Producto producto) {
        return this.repositorioProducto.save(producto);
    }

    @Override
    public void borrarProductoPorId(String id) {
        this.repositorioProducto.deleteById(id);
    }
}
