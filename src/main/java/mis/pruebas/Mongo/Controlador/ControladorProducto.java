package mis.pruebas.Mongo.Controlador;

import mis.pruebas.Mongo.Modelo.Producto;
import mis.pruebas.Mongo.Servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v2/productos")

public class ControladorProducto {

    @Autowired
    ServicioProducto servicioProducto;

    @GetMapping()
    public List<Producto> obtenerProductos() {
       return this.servicioProducto.obtenerProductos();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Producto agregarProducto(@RequestBody Producto producto){
        return this.servicioProducto.crearProducto(producto);
    }

   @GetMapping("/{id}")
    public ResponseEntity<Producto> obtenerProducto(@PathVariable String id){
        Optional<Producto> p = this.servicioProducto.obtenerProductoPorId(id);
        if(p.isPresent()){
            return ResponseEntity.ok(p.get());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borraProducto(@PathVariable String id){
        this.servicioProducto.borrarProductoPorId(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Producto ModificaProducto(@RequestBody Producto producto){
        return this.servicioProducto.actualizarProducto(producto);
    }
}
